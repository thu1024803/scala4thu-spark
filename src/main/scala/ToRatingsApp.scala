import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac028 on 2017/5/15.
  */
object ToRatingsApp extends App {

  val conf = new SparkConf().setAppName("TopRatings")
  .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val ratings : RDD[(Int,Double)]=sc.textFile("/Users/mac028/Downloads/ml-20m/ratings.csv")
  .map(str=>str.split(","))
  .filter(strs=>strs(1)!="movieId")
  .map(strs=>
  (strs(1).toInt,strs(2).toDouble))


  val movies=sc.textFile("/Users/mac028/Downloads/ml-20m/ratings.csv/movies.csv")
    .map(str=>str.split(","))
    .filter(strs=>strs(0)!="movieID")
    .map(strs=>strs(0).toInt->strs(1))
  movies.take(10).foreach(println)







  val joined: RDD[(Int, (Double, String))] =averageRatingByMovieId.join(movies)



  joined.takeSample(false,5).foreach(println)




  





  //val top10 =averageRatingByMovieId.sortBy(_._2,false).take(10)
  //top10.foreach(println)


  //val totalRatingByMovieID=ratings.reduceByKey((acc,curr)=>acc+curr)
  //totalRatingByMovieID.takeSample(false,5).foreach(println)


  //ratings.take(10).foreach(println)

}
